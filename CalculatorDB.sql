USE [Calculator]
GO
/****** Object:  Table [dbo].[Calculation]    Script Date: 24-11-2020 15:05:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calculation](
	[CalculationID] [int] IDENTITY(1,1) NOT NULL,
	[CalculatedOn] [datetime] NULL,
	[Status] [nchar](10) NULL,
 CONSTRAINT [PK_Calculation] PRIMARY KEY CLUSTERED 
(
	[CalculationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operand]    Script Date: 24-11-2020 15:05:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operand](
	[OperandID] [int] IDENTITY(1,1) NOT NULL,
	[Value] [float] NULL,
	[CalculationID] [int] NULL,
 CONSTRAINT [PK_Operand] PRIMARY KEY CLUSTERED 
(
	[OperandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operation]    Script Date: 24-11-2020 15:05:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operation](
	[OperationID] [int] IDENTITY(1,1) NOT NULL,
	[OperatorID] [int] NULL,
	[CalculationID] [int] NULL,
 CONSTRAINT [PK_Operation] PRIMARY KEY CLUSTERED 
(
	[OperationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operator]    Script Date: 24-11-2020 15:05:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operator](
	[OperatorID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NULL,
	[Symbol] [varchar](10) NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_Operator] PRIMARY KEY CLUSTERED 
(
	[OperatorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Operator] ON 

INSERT [dbo].[Operator] ([OperatorID], [Name], [Symbol], [Description]) VALUES (3, N'Add', N'+', N'To perform addition operation')
INSERT [dbo].[Operator] ([OperatorID], [Name], [Symbol], [Description]) VALUES (4, N'Multiply', N'x', N'To perform mulitiplication')
INSERT [dbo].[Operator] ([OperatorID], [Name], [Symbol], [Description]) VALUES (5, N'Divide', N'÷', N'To perform division')
INSERT [dbo].[Operator] ([OperatorID], [Name], [Symbol], [Description]) VALUES (6, N'Subtraction', N'-', N'To perform substraction')
SET IDENTITY_INSERT [dbo].[Operator] OFF
ALTER TABLE [dbo].[Operand]  WITH CHECK ADD  CONSTRAINT [FK_Operand_Calculation] FOREIGN KEY([CalculationID])
REFERENCES [dbo].[Calculation] ([CalculationID])
GO
ALTER TABLE [dbo].[Operand] CHECK CONSTRAINT [FK_Operand_Calculation]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Operation_Calculation] FOREIGN KEY([CalculationID])
REFERENCES [dbo].[Calculation] ([CalculationID])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Operation_Calculation]
GO
ALTER TABLE [dbo].[Operation]  WITH CHECK ADD  CONSTRAINT [FK_Operation_Operator] FOREIGN KEY([OperatorID])
REFERENCES [dbo].[Operator] ([OperatorID])
GO
ALTER TABLE [dbo].[Operation] CHECK CONSTRAINT [FK_Operation_Operator]
GO
